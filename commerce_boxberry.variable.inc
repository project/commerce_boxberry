<?php

function commerce_boxberry_variable_group_info() {
  $groups = array();

  $groups['commerce_boxberry_connection_settings'] = array(
    'title' => t('Connection settings'),
    'access' => 'boxberry connection settings',
  );
  $groups['commerce_boxberry_order_tokens'] = array(
    'title' => t('Order info tokens settings'),
    'access' => 'boxberry order tokens settings',
  );
  $groups['commerce_boxberry_order_line_items'] = array(
    'title' => t('Order info line items settings'),
    'access' => 'boxberry order line items settings',
  );

  return $groups;
}

function commerce_boxberry_variable_info() {
  $variables = array();

  $variables['commerce_boxberry_server_key'] = array(
    'title' => t('API Key'),
    'group' => 'commerce_boxberry_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 16,
    ),
    'type' => 'string',
    'token' => TRUE,
    'access' => 'boxberry connection settings',
  );
  $variables['commerce_boxberry_server_is_test_mode'] = array(
    'title' => t('Test Mode'),
    'group' => 'commerce_boxberry_connection_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'type' => 'boolean',
    'default' => TRUE,
    'token' => TRUE,
    'access' => 'boxberry connection settings',
  );

  $variables['commerce_boxberry_tokens_order_id'] = array(
    'title' => t('Order Id'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:order-id]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_customer_fio'] = array(
    'title' => t('Customer Full Name'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:name-line]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_customer_phone'] = array(
    'title' => t('Customer Phone'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:field-customer-phone]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_customer_email'] = array(
    'title' => t('Customer Email'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:mail]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_customer_address'] = array(
    'title' => t('Customer Address'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:thoroughfare]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_customer_city'] = array(
    'title' => t('Customer City'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:locality]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_customer_postal_code'] = array(
    'title' => t('Customer PostCode'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:postal-code]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_pickpoint_from'] = array(
    'title' => t('Warehouse City Code'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'type' => 'string',
    'default' => '',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_payment_sum'] = array(
    'title' => t('Amount to be collected'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:order-payment-balance-amount-decimal]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_delivery_sum'] = array(
    'title' => t('Order delivery price'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:order-payment-balance-amount-decimal]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_price'] = array(
    'title' => t('Order Valuated Amount (order total)'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-order-total:amount-decimal]',
    'access' => 'boxberry order tokens settings',
  );
  //$variables['commerce_boxberry_tokens_Weight'] = array(
  //  'title' => t('Order Weight'),
  //  'group' => 'commerce_boxberry_order_tokens',
  //  'element' => array(
  //    '#type' => 'textfield',
  //    '#size' => 120,
  //  ),
  //  'type' => 'string',
  //  'default' => '1',
  //  'access' => 'boxberry order tokens settings',
  //);
  //$variables['commerce_boxberry_tokens_Volume'] = array(
  //  'title' => t('Number Of Allocated spaces'),
  //  'group' => 'commerce_boxberry_order_tokens',
  //  'element' => array(
  //    '#type' => 'textfield',
  //    '#size' => 120,
  //  ),
  //  'type' => 'string',
  //  'default' => '1',
  //  'access' => 'boxberry order tokens settings',
  //);
  $variables['commerce_boxberry_tokens_delivery_date'] = array(
    'title' => t('Delivery date'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-order-delivery-date:value:custom:d-m-Y]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_time_from'] = array(
    'title' => t('Delivery Time period beginning'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-order-delivery-date:value:custom:H-i]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_time_to'] = array(
    'title' => t('Delivery Time period end'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-order-delivery-date:value2:custom:H-i]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_ServicePointExternalCode'] = array(
    'title' => t('Service point ExternalCode'),
    'description' => t('If order has non-empty code then order in considered for delivery to service point but not to customer address.'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:commerce-customer-shipping:field-service-point:external-code]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_barcode'] = array(
    'title' => t('Order BarCode'),
    'description' => t('Barcode which is used to track the order.'),
    'group' => 'commerce_boxberry_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '[commerce-order:field-parcel-code]',
    'access' => 'boxberry order tokens settings',
  );
  $variables['commerce_boxberry_tokens_product_weight'] = array(
    'title' => t('Product Weight'),
    'group' => 'commerce_boxberry_order_line_items',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'type' => 'string',
    'default' => '300',
    'access' => 'boxberry order tokens settings',
  );


  return $variables;
}
