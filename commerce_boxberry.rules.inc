<?php

/**
 * @file
 * Boxberry Rules hooks, actions and triggers.
 */

function commerce_boxberry_rules_event_info() {
  $events = array();

  $events['commerce_boxberry_order_submitted'] = array(
    'label' => t('Order just been submitted to Boxberry system'),
    'group' => t('Commerce Boxberry'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce order'),
      ),
      'code' => array(
        'type' => 'text',
        'label' => t('Boxberry order tracking code'),
      ),
      'is_test_mode' => array(
        'type' => 'boolean',
        'label' => t('Is test mode'),
        'default value' => FALSE,
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );
  //$events['commerce_boxberry_order_state_changed'] = array(
  //  'label' => t('Order state changed in Boxberry system'),
  //  'group' => t('Commerce Boxberry'),
  //  'variables' => array(
  //    'commerce_order' => array(
  //      'type' => 'commerce_order',
  //      'label' => t('Commerce order'),
  //    ),
  //    'order_info' => array(
  //      'type' => 'struct',
  //      'label' => t('Boxberry system order info'),
  //    ),
  //    'is_test_mode' => array(
  //      'type' => 'boolean',
  //      'label' => t('Is test mode'),
  //      'default value' => FALSE,
  //    ),
  //  ),
  //  'access callback' => 'commerce_order_rules_access',
  //);
  //$events['commerce_boxberry_service_point_changed'] = array(
  //  'label' => t('Service point information changed in Boxberry'),
  //  'group' => t('Commerce Boxberry'),
  //  'variables' => array(
  //    'service_point' => array(
  //      'type' => 'struct',
  //      'label' => t('Service point'),
  //    ),
  //  ),
  //  'access callback' => 'commerce_order_rules_access',
  //);

  return $events;
}


function commerce_boxberry_rules_action_info() {
  $actions = array();

  $actions['commerce_boxberry_submit_order'] = array(
    'label' => t('Submit order to Boxberry system.'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'save' => FALSE,
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_boxberry_submit_order_rules_action',
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $actions;
}

function commerce_boxberry_submit_order_rules_action($commerce_order, $settings, RulesState $state, RulesPlugin $element) {
  $result = commerce_boxberry_submit_order($commerce_order);
  if ($result && isset($result['Code'])) {
    watchdog('commerce_boxberry', 'Successfully submit order @order_id', array('@order_id' => $commerce_order->order_id), WATCHDOG_INFO);
  }
  elseif ($result && isset($result['Error'])) {
    watchdog('commerce_boxberry', 'Failed to register order @order_id: "@message"', array(
      '@order_id' => $commerce_order->order_id,
      '@message' => $result['Error'],
    ), WATCHDOG_WARNING);
  }
  else {
    watchdog('commerce_boxberry', 'Order is not compatible with Boxberry processing "<a href ="/admin/commerce/orders/@order_id/edit">@order_id</a>"', array(
      '@order_id' => $commerce_order->order_id,
    ), WATCHDOG_WARNING);
  }
}